#include "Hantek.h"
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <exception>
#include <stdexcept>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include "Timer.h"

wxMutex* CHantek::s_mutexReadBuffer = NULL;
wxMutex* CHantek::s_mutexWriteBuffer = NULL;

CHantek::CHantek() {
    if (s_mutexReadBuffer == NULL) s_mutexReadBuffer = new wxMutex();
    if (s_mutexWriteBuffer == NULL) s_mutexWriteBuffer = new wxMutex();

    m_pDevice = NULL;
    m_pContext = NULL;
    m_pHandle = NULL;
    m_nAttached = 0;
}

CHantek::~CHantek() { close(); }

void CHantek::init(void) {
    int i, ret;
    ssize_t cnt;

    ret = libusb_init(&m_pContext);
    if (ret) {
        printf("cannot init libusb: %s\n", libusb_error_name(ret));
        return;
    }

    cnt = libusb_get_device_list(m_pContext, &list);
    if (cnt <= 0) {
        printf("no devices found\n");
        return;
    }

    for (i = 0; i < cnt; ++i) {
        libusb_device* dev = list[i];
        struct libusb_device_descriptor desc;
        ret = libusb_get_device_descriptor(dev, &desc);
        if (ret) {
            printf("unable to get device descriptor: %s\n",
                   libusb_error_name(ret));
            exit(-1);
        }

        if (desc.idVendor == VENDOR && desc.idProduct == PRODUCT) {
            m_pDevice = dev;
            break;
        }
    }

    if (!m_pDevice) {
        printf("no devices found\n");
        return;
    }

    ret = libusb_open(m_pDevice, &m_pHandle);
    if (ret) {
        printf("cannot open device: %s\n", libusb_error_name(ret));
        return;
    }

    if (libusb_claim_interface(m_pHandle, 0)) {
        ret = libusb_detach_kernel_driver(m_pHandle, 0);
        if (ret) {
            printf("unable to detach kernel driver: %s\n",
                   libusb_error_name(ret));
            return;
        }

        m_nAttached = 1;
        ret = libusb_claim_interface(m_pHandle, 0);
        if (ret) {
            printf("cannot claim interface: %s\n", libusb_error_name(ret));
            return;
        }
    }
}

void CHantek::close(void) {
    if (m_nAttached == 1) libusb_attach_kernel_driver(m_pHandle, 0);

    if (m_pHandle) libusb_close(m_pHandle);

    libusb_free_device_list(list, 1);

    libusb_exit(m_pContext);
}

void CHantek::readData(void) {
    uint8_t data[100000];
    int transferred;
    int result;

    do {
        result = libusb_bulk_transfer(m_pHandle, EP_BULK_IN, data, sizeof(data), &transferred, TIMEOUT_MS);

        if (result >= 0) {  // successful read
            printf("\nReading %d bytes\n", transferred);

            wxMutexLocker lock(*s_mutexReadBuffer);

            m_ReadBuffer.insert(m_ReadBuffer.end(), data, data + transferred);
        } else if (result != LIBUSB_ERROR_TIMEOUT) {
            printf("read error: %s\n", libusb_error_name(result));
            //return false;
        }
    } while(result == 0);

    //getNextPacket();
}

void CHantek::writePacket(CHantekPacket& packet) {
    printf("\nWriting Packet");
    packet.print();

    m_WriteBuffer.insert(m_WriteBuffer.end(), packet.getPacketData(), packet.getPacketData() + packet.getPacketSize());

    CTimer timer(1000);
    while (!timer.isTimeout() && (m_WriteBuffer.size() > 0)) {
        //int length = (m_WriteBuffer.size() > 64) ? 64 : m_WriteBuffer.size();  // send max size of 64 bytes
        int length = m_WriteBuffer.size();

        printf("\nWriting %d bytes\n", length);

        int transferred;
        int nResult = libusb_bulk_transfer(m_pHandle, EP_BULK_OUT, m_WriteBuffer.data(), length, &transferred, TIMEOUT_MS);

        if (nResult == 0) {  // successful write
            wxMutexLocker lock(*s_mutexWriteBuffer);

            // remove the bytes that were written
            m_WriteBuffer.erase(m_WriteBuffer.begin(), m_WriteBuffer.begin() + transferred);
        } else {
            printf("Write error: %s\n", libusb_error_name(nResult));
            break;
        }
    }
}

bool CHantek::getNextPacket(void) {
    CTimer timer(1000);
    while (!timer.isTimeout()) {
        readData();

        bool bFound = false;

        // find valid packet
        while (m_ReadBuffer.size() >= 5) {
            // make sure we have enought bytes read for packet
            if (CHantekPacket::getPacketLength(m_ReadBuffer.data(),
                                               m_ReadBuffer.size()) >
                m_ReadBuffer.size()) {
                return false;
            }

            if (CHantekPacket::validate(m_ReadBuffer.data(),
                                        m_ReadBuffer.size())) {
                bFound = true;
                break;
            } else {
                /*printf("\nInvalid Packet %02x %02x %02x %02x %02x", pData2[0],
                       pData2[1], pData2[2], pData2[3], pData2[4]);*/
                printf("\nInvalid Packet, Skipping byte\n");
                m_ReadBuffer.erase(m_ReadBuffer.begin());
            }
        }

        if (!bFound) {
            return false;
        }

        m_Packet.setPacket(m_ReadBuffer.data(), m_ReadBuffer.size());

        printf("\ngetNextPacket Found Packet\n");
        m_Packet.print();

        m_ReadBuffer.erase(m_ReadBuffer.begin(),
                           m_ReadBuffer.begin() +
                               CHantekPacket::getPacketLength(
                                   m_ReadBuffer.data(), m_ReadBuffer.size()));

        return true;
    }

    return false;
}

void CHantek::handlePacket(CHantekPacket& packet) {
    if (!packet.validate()) {
        throw std::runtime_error("Invalid Packet");
    }
}

void CHantek::echo(void) {
    CHantekPacket packet;
    
    packet.setCommand(0x00);

    /*uint8_t nDataBytes[] = {1, 2, 3, 4};
    write(NORMAL_MSG, 0x21, 0, NULL);  // sizeof(nDataBytes), nDataBytes);
    // write(NORMAL_MSG, 0x20, 0, NULL);
    printf("\nSent\n");
    print();

    read();
    printf("\nRead\n");
    // print();
    while (1) {
        read();
        getNextPacket();
        print();
    }*/
}

bool CHantek::readFile(const char* nFileName) {
    printf("\nRead File %s\n", nFileName);

    CHantekPacket packet;

    packet.setMarker(NORMAL_MSG);
    packet.setCommand(0x10);

    uint16_t nLen = strlen(nFileName) + 1;

    uint8_t buf[nLen];

    buf[0] = 0x00;
    memcpy(&buf[1], nFileName, strlen(nFileName));

    packet.setData(buf, nLen);
    packet.print();

    m_FileData.clear();
    m_ReadFileState = FS_DOWNLOADING;

    writePacket(packet);

    CTimer timer(1000);
    while (!timer.isTimeout()) {
        if (getNextPacket()) {
            if ((m_Packet.getCommand() == 0x90) && (m_Packet.getSubcommand() == 0x01)) {  // file contents
                m_FileData.insert(m_FileData.end(), m_Packet.getData() + 1, m_Packet.getData() + m_Packet.getDataLength());
            } else if ((m_Packet.getCommand() == 0x90) && (m_Packet.getSubcommand() == 0x02)) {  // eof with checksum of all data
                printf("\nDone Reading File\n");

                uint32_t nChecksum = 0;
                for (size_t i = 0; i < m_FileData.size(); i++) {
                    printf("%c", m_FileData[i]);
                    nChecksum += m_FileData[i];
                }

                if ((nChecksum & 0xff) == m_Packet.getData()[1]) {
                    return true;
                } else {
                    printf("\nInvalid File Checksum\n");
                    return false;
                }
            } else {
                printf("\nUnknown Read File Response %02x\n", m_Packet.getCommand());
                m_Packet.print();
            }
        }
    }

    return false;
}

bool CHantek::keypressTrigger(uint8_t key) {
    printf("\nCHantek::keypressTrigger\n");

    CHantekPacket packet;

    packet.setMarker(NORMAL_MSG);
    packet.setCommand(0x13);

    uint8_t nDataBytes[] = {key, 0x01};
    packet.setData(nDataBytes, 2);
    writePacket(packet);

    CTimer timer(1000);
    while(!timer.isTimeout()) {
        if (getNextPacket()) {
            if (m_Packet.getCommand() == 0x93) {
                return true;
            } else {
                printf("\nUnknown Keypress Trigger Response\n");
                m_Packet.print();
            }
        }

        timer.reset();
    }

    return false;
}

bool CHantek::getLine(std::string& buffer, const std::vector<uint8_t>& data,
                      std::vector<uint8_t>::iterator& iter) {
    if (iter == data.end()) {
        return false;
    }

    buffer.clear();

    while (iter != data.end()) {
        if (*iter == '\r') {
            // ignore carriage return, treat as unix file
        } else if (*iter == '\n') {
            iter++;
            break;
        } else {
            buffer += *iter;
        }

        iter++;
    }

    return true;
}

bool sp(std::string from, std::string& to, uint8_t& len) {
    to.clear();
    len = 0;

    std::size_t index = from.find(']');

    if (index == std::string::npos) {
        return false;
    }

    to = from.substr(0, index + 1);
    from.erase(0, index + 1);

    // left trim
    index = from.find_first_not_of(" \t");
    if (index != std::string::npos) {
        from = from.substr(index);
    }

    if (!from.empty()) {
        len = std::stoi(from);
    }

    return true;
}

bool CHantek::loadProtocol(void) {
    if (!readFile("/protocol.inf")) {
        printf("\nCant read /protocol.inf\n");
        return false;
    }

    std::string buf, tag;

    std::vector<uint8_t>::iterator iter = m_FileData.begin();
    while (getLine(buf, m_FileData, iter)) {
        if (buf.empty()) {
            continue;
        }

        SysDataField field;

        sp(buf, field.sField, field.nLength);

        if (field.sField.empty() || (field.sField.compare("[TOTAL]") == 0) ||
            (field.sField.compare("[START]") == 0) ||
            (field.sField.compare("[END]") == 0)) {
            continue;
        }

        m_lstSysDataFields.push_back(field);
    }

    std::vector<SysDataField>::iterator i2 = m_lstSysDataFields.begin();
    while (i2 != m_lstSysDataFields.end()) {
        printf("\n%s %u", (*i2).sField.c_str(), (*i2).nLength);
        i2++;
    }

    return true;
}

bool CHantek::readDSOSettings(void) {
    printf("\nRead DSO Setting\n");

    if (m_lstSysDataFields.empty()) {
        loadProtocol();
    }

    CHantekPacket packet;
    packet.setMarker(NORMAL_MSG);
    packet.setCommand(0x01);

    writePacket(packet);

    CTimer timer(1000);
    while(!timer.isTimeout()) {
        if (getNextPacket()) {
            if (m_Packet.getCommand() == 0x81) {
                if (m_Packet.getDataLength() <= 0) {
                    printf("\nCHantek::handleReadDSOSettings No SYSData");
                    return false;
                }

                m_DSOSettings.clear();
                m_DSOSettings.insert(m_DSOSettings.begin(), m_Packet.getData(), m_Packet.getData() + m_Packet.getDataLength());

                printf("\nCHantek::handleReadDSOSettings Got DSO Settings ");
                for (int i = 0; i < m_DSOSettings.size(); i++) {
                    printf("%02x ", m_DSOSettings[i]);
                }

                return true;
            } else {
                printf("\nUnknown Read DSO Settings Response\n");
                m_Packet.print();
            }
        }
    }

    return false;
}

bool CHantek::getDSOSetting(const std::string& setting, uint8_t* buf,
                            uint8_t& size) {
    if (m_DSOSettings.size() <= 0) {
        if (!readDSOSettings()) return false;
    }

    uint16_t nOffset = 0;

    std::vector<SysDataField>::iterator iter = m_lstSysDataFields.begin();
    while (iter != m_lstSysDataFields.end()) {
        if ((*iter).sField.compare(setting) == 0) {
            memcpy(buf, &m_DSOSettings[nOffset], iter->nLength);
            return true;
        }

        nOffset += iter->nLength;

        iter++;
    }
printf("\nsetting not found %s\n", setting.c_str());
    return false;
}

uint32_t CHantek::getMilliVoltsPerDiv(uint8_t channel) {
    uint8_t nVertVB;
    uint16_t nMVoltsPerDiv;
    uint8_t nProbe;
    uint16_t nMult;
    uint8_t nLen;

    uint16_t nMVoltsDiv = 0;

    if (channel == CHANNEL_1) {
        if (!getDSOSetting("[VERT-CH1-VB]", &nVertVB, nLen)) return 0;
        if (!getDSOSetting("[VERT-CH1-PROBE]", &nProbe, nLen)) return 0;
    } else if (channel == CHANNEL_2) {
        if (!getDSOSetting("[VERT-CH2-VB]", &nVertVB, nLen)) return 0;
        if (!getDSOSetting("[VERT-CH2-PROBE]", &nProbe, nLen)) return 0;
    }

    switch (nVertVB) {
        case 0x00:
            nMVoltsPerDiv = 2;
            break;
        case 0x01:
            nMVoltsPerDiv = 5;
            break;
        case 0x02:
            nMVoltsPerDiv = 10;
            break;
        case 0x03:
            nMVoltsPerDiv = 20;
            break;
        case 0x04:
            nMVoltsPerDiv = 50;
            break;
        case 0x05:
            nMVoltsPerDiv = 100;
            break;
        case 0x06:
            nMVoltsPerDiv = 200;
            break;
        case 0x07:
            nMVoltsPerDiv = 500;
            break;
        case 0x08:
            nMVoltsPerDiv = 1000;
            break;
        case 0x09:
            nMVoltsPerDiv = 2000;
            break;
        case 0x0A:
            nMVoltsPerDiv = 5000;
            break;
        default:
            return 0;
    }

    switch (nProbe) {
        case 0x00:
            nMult = 1;
            break;
        case 0x01:
            nMult = 10;
            break;
        case 0x02:
            nMult = 100;
            break;
        case 0x03:
            nMult = 1000;
            break;
        default:
            return 0;
    }

    return nMVoltsPerDiv * nMult;
}

std::string CHantek::getChanDisplay(uint8_t channel) {
    uint8_t nBuf;
    uint8_t nLen;

    if (channel == CHANNEL_1) {
        if (!getDSOSetting("[VERT-CH1-DISP]", &nBuf, nLen)) return "";
    } else if (channel == CHANNEL_2) {
        if (!getDSOSetting("[VERT-CH2-DISP]", &nBuf, nLen)) return "";
    }

    switch (nBuf) {
        case 0x00:
            return "Off";
        case 0x01:
            return "On";
    }

    return "";
}

std::string CHantek::getCoupling(uint8_t channel) {
    uint8_t nBuf;
    uint8_t nLen;

    if (channel == CHANNEL_1) {
        if (!getDSOSetting("[VERT-CH1-COUP]", &nBuf, nLen)) return "";
    } else if (channel == CHANNEL_2) {
        if (!getDSOSetting("[VERT-CH2-COUP]", &nBuf, nLen)) return "";
    }

    switch (nBuf) {
        case 0x00:
            return "DC";
        case 0x01:
            return "AC";
        case 0x02:
            return "GND";
    }

    return "";
}
