#ifndef __HANTEK_H__
#define __HANTEK_H__

#include <libusb-1.0/libusb.h>
#include <string>
#include <vector>
#include <wx/thread.h>
#include "HantekPacket.h"

#define TIMEOUT_MS 100

#define VENDOR 0x049F
#define PRODUCT 0x505A

#define EP_BULK_IN (1 | LIBUSB_ENDPOINT_IN)
#define EP_BULK_OUT (2 | LIBUSB_ENDPOINT_OUT)

#define KEY_F0 0x00
#define KEY_F1 0x01
#define KEY_F2 0x02
#define KEY_F3 0x03
#define KEY_F4 0x04
#define KEY_F5 0x05
#define KEY_F6 0x06
#define KEY_F7 0x07
#define KEY_V0_CCW 0x08
#define KEY_V0_CW 0x09
#define KEY_V0_C 0x0A
#define KEY_SAVE_RECALL 0x0B
#define KEY_MEASURE 0x0C
#define KEY_ACQUIRE 0x0D
#define KEY_UTILITY 0x0E
#define KEY_CURSOR 0x0F
#define KEY_DISPLAY 0x10
#define KEY_AUTOSET 0x11
#define KEY_SING_SEQ 0x12
#define KEY_RUN_STOP 0x13
#define KEY_HELP 0x14
#define KEY_DEF_SETUP 0x15
#define KEY_SAVE_USB 0x16
#define KEY_MATH_MENU 0x17
#define KEY_CH1_MENU 0x18
#define KEY_CH1_POS_CCW 0x19
#define KEY_CH1_POS_CW 0x1A
#define KEY_CH1_POS_C 0x1B
#define KEY_CH1_VOLTS_DIV_CCW 0x1C
#define KEY_CH1_VOLTS_DIV_CW 0x1D
#define KEY_CH2_MENU 0x1E
#define KEY_CH2_POS_CCW 0x1F
#define KEY_CH2_POS_CW 0x20
#define KEY_CH2_POS_C 0x21
#define KEY_CH2_VOLTS_DIV_CCW 0x22
#define KEY_CH2_VOLTS_DIV_CW 0x23
#define KEY_HORIZ_MENU 0x24
#define KEY_HORIZ_POS_CW 0x25
#define KEY_HORIZ_POS_CCW 0x26
#define KEY_HORIZ_POS_C 0x27
#define KEY_HORIZ_SEC_DIV_CCW 0x28
#define KEY_HORIZ_SEC_DIV_CW 0x29
#define KEY_TRIG_MENU 0x2A
#define KEY_TRIG_LEVEL_CCW 0x2B
#define KEY_TRIG_LEVEL_CW 0x2C
#define KEY_TRIG_LEVEL_C 0x2D
#define KEY_SET_TO_50 0x2E
#define KEY_FORCE_TRIG 0x2F
#define KEY_PROBE_CHECK 0x30

#define CHANNEL_1 0
#define CHANNEL_2 1

enum FileState { FS_NONE, FS_DOWNLOADING, FS_COMPLETE, FS_INVALID_CHECKSUM };

struct SysDataField {
    std::string sField;
    uint8_t nLength;
};

class CHantek {
   public:
    static wxMutex* s_mutexReadBuffer;
    static wxMutex* s_mutexWriteBuffer;

    CHantek();
    virtual ~CHantek();
    void init(void);
    void close(void);
    void handlePacket(CHantekPacket& packet);
    /*bool read(void);
    bool write(uint8_t nMarker, uint8_t nCmd, uint16_t nDataLen,
               uint8_t* pData);*/
    void echo(void);

    bool readFile(const char* nFileName);
    bool keypressTrigger(uint8_t key);
    bool getNextPacket(void);

    bool loadProtocol(void);
    bool readDSOSettings(void);
    bool getDSOSetting(const std::string& setting, uint8_t* buf, uint8_t& size);
    uint32_t getMilliVoltsPerDiv(uint8_t channel);
    std::string getChanDisplay(uint8_t channel);
    std::string getCoupling(uint8_t channel);

   protected:
    libusb_device* m_pDevice;
    libusb_context* m_pContext;
    libusb_device_handle* m_pHandle;
    libusb_device** list;
    int m_nAttached;
    std::vector<uint8_t> m_ReadBuffer;
    std::vector<uint8_t> m_WriteBuffer;
    std::vector<uint8_t> m_DSOSettings;
    std::vector<uint8_t> m_FileData;
    std::vector<SysDataField> m_lstSysDataFields;
    CHantekPacket m_Packet;
    FileState m_ReadFileState;

    void readData(void);
    void writePacket(CHantekPacket& packet);

    bool getLine(std::string& buffer, const std::vector<uint8_t>& data,
                 std::vector<uint8_t>::iterator& iter);
};

#endif