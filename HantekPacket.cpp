#include "HantekPacket.h"
#include <exception>
#include <stdexcept>

CHantekPacket::CHantekPacket() {
    setLength(0);
}

CHantekPacket::~CHantekPacket() {
    
}

void CHantekPacket::setChecksum(void) {
    m_Data[getLength()+2] = calcChecksum(m_Data.data(), m_Data.size());
}

bool CHantekPacket::setPacket(uint8_t* pData, uint32_t nMaxLen) {
    printf("\nset packet size %u", nMaxLen);

    if (!validate(pData, nMaxLen)) {
        return false;
    }

    m_Data.clear();
    m_Data.insert(m_Data.begin(), pData, pData + getPacketLength(pData, nMaxLen));

    if (!validate()) {
        throw std::runtime_error("Invalid Packet");
    }

    return true;
}

uint8_t CHantekPacket::getMarker(void) {
    if (m_Data.size() <= 0) {
        return 0;
    }

    return m_Data[0];
}

CHantekPacket* CHantekPacket::setMarker(uint8_t nMarker) {
    m_Data[0] = nMarker;

    setChecksum();

    return this;
}

uint16_t CHantekPacket::getLength(void) {
    return getLength(m_Data.data(), m_Data.size());
}

uint16_t CHantekPacket::getDataLength(void) {
    return getDataLength(m_Data.data(), m_Data.size());
}

CHantekPacket* CHantekPacket::setLength(uint16_t nLen) {
    if (nLen > 0xFD) {
        throw std::length_error("Invalid Length");
    }

    // add command and checksum byte
    nLen += 2;

    // allociate space
    m_Data.resize(nLen + 3);

    m_Data[1] = 0xFF & nLen;
    m_Data[2] = 0xFF & (nLen >> 8);

    setChecksum();

    return this;
}

uint8_t CHantekPacket::getCommand(void) {
    if (m_Data.size() < 4) {
        return 0;
    }

    return m_Data[3];
}

CHantekPacket* CHantekPacket::setCommand(uint8_t nCmd) {
    m_Data[3] = nCmd;

    setChecksum();

    return this;
}

uint8_t CHantekPacket::getSubcommand(void) {
    if (m_Data.size() < 5) {
        return 0;
    }

    return m_Data[4];
}

CHantekPacket* CHantekPacket::setSubcommand(uint8_t nCmd) {
    m_Data[4] = nCmd;

    setChecksum();

    return this;
}

uint8_t* CHantekPacket::getData(void) {
    return &(m_Data.data()[4]);
}

CHantekPacket* CHantekPacket::setData(uint8_t* pData, uint16_t nLen) {
    setLength(nLen);

    for (uint8_t i = 0; i < nLen; i++) {
        m_Data[4+i] = pData[i];
    }
    
    setChecksum();

    return this;
}

uint8_t* CHantekPacket::getPacketData(void) {
    return m_Data.data();
}

uint32_t CHantekPacket::getPacketSize(void) {
    return m_Data.size();
}

bool CHantekPacket::validate(void) {
    if (!validate(m_Data.data(), m_Data.size())) {
        return false;
    }

    if (getPacketLength(m_Data.data(), m_Data.size()) != m_Data.size()) {
        printf("\nInvalid Buffer Length");

        return false;
    }

    return true;
}

void CHantekPacket::print(void) {
    printf("\n");

    for (size_t i = 0; i < m_Data.size(); i++) {
        printf(" %02x", m_Data[i]);
    }

    printf("\nBuffer Size %ld  Length %u  Data Length %u  Command %02x\n\n", m_Data.size(), getLength(), getDataLength(), getCommand());

    /*printf("  Command: %02x  Checksum %02x  Calc Checksum %02x  Length: %d\n",
           getCommand(), getChecksum(), calcChecksum(), getLength());*/

}

uint32_t CHantekPacket::getPacketLength(uint8_t* pData, uint32_t nMaxLen) {
    return getLength(pData, nMaxLen) + 3;
}

uint16_t CHantekPacket::getLength(uint8_t* pData, uint32_t nMaxLen) {
    if (nMaxLen < 5) {
        throw std::out_of_range("Invalid Length");
    }

    return (pData[2] << 8) | pData[1];
}

uint16_t CHantekPacket::getDataLength(uint8_t* pData, uint32_t nMaxLen) {
    return getLength(pData, nMaxLen) - 2;
}

uint8_t CHantekPacket::getChecksum(uint8_t* pData, uint32_t nMaxLen) {
    if (getPacketLength(pData, nMaxLen) > nMaxLen) {
        throw std::out_of_range("Invalid Packet Length");
    }

    return pData[getPacketLength(pData, nMaxLen) - 1];
}

uint8_t CHantekPacket::calcChecksum(uint8_t* pData, uint32_t nMaxLen) {
    if (getPacketLength(pData, nMaxLen) > nMaxLen) {
        throw std::out_of_range("Invalid Packet Length");
    }

    uint32_t nChecksum = 0;

    for (uint32_t i = 0; i < (uint32_t) getPacketLength(pData, nMaxLen)-1; i++) {
        nChecksum += pData[i];
    }

    return (uint8_t)(0xff & nChecksum);
}

bool CHantekPacket::validate(uint8_t* pData, uint32_t nMaxLen) {
    // must contain at a minimum - marker lsb usb cmd checksum
    if (nMaxLen < 5) {
        printf("\nInvalid Length %u", nMaxLen);

        return false;
    }

    // must start with this marker
    if ((pData[0] != NORMAL_MSG) && (pData[0] != DEBUG_MSG)) {
        printf("\nInvalid Marker %02x", pData[0]);

        return false;
    }

    // check if has valid packet length
    if (getPacketLength(pData, nMaxLen) > nMaxLen) {
        printf("\nInvalid Packet Length %u > %u", getPacketLength(pData, nMaxLen), nMaxLen);

        return false;
    }

    if (calcChecksum(pData, nMaxLen) != getChecksum(pData, nMaxLen)) {
        printf("\nInvalid Checksum Calc %02x Set %02x", calcChecksum(pData, nMaxLen), getChecksum(pData, nMaxLen));

        return false;
    }

    return true;
}