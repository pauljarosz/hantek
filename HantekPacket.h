#ifndef __HANTEKPACKET_H__
#define __HANTEKPACKET_H__

#include <string>
#include <vector>

#define NORMAL_MSG 0x53
#define DEBUG_MSG 0x43

class CHantekPacket {
   public:
    CHantekPacket();
    virtual ~CHantekPacket();
    bool setPacket(uint8_t* pData, uint32_t nMaxLen);
    uint8_t getMarker(void);
    CHantekPacket* setMarker(uint8_t nMarker);
    uint16_t getLength(void);
    uint16_t getDataLength(void);
    CHantekPacket* setLength(uint16_t nLen);
    uint8_t getCommand(void);
    CHantekPacket* setCommand(uint8_t nCmd);
    uint8_t getSubcommand(void);
    CHantekPacket* setSubcommand(uint8_t nCmd);
    uint8_t* getData(void);
    CHantekPacket* setData(uint8_t* pData, uint16_t nLen);
    uint8_t* getPacketData(void);
    uint32_t getPacketSize(void);
    bool validate(void);
    void print(void);

    static uint32_t getPacketLength(uint8_t* pData, uint32_t nMaxLen);
    static uint16_t getLength(uint8_t* pData, uint32_t nMaxLen);
    static uint16_t getDataLength(uint8_t* pData, uint32_t nMaxLen);
    static uint8_t getChecksum(uint8_t* pData, uint32_t nMaxLen);
    static uint8_t calcChecksum(uint8_t* pData, uint32_t nMaxLen);
    static bool validate(uint8_t* pData, uint32_t nMaxLen);

   protected:
    std::vector<uint8_t> m_Data;
    void setChecksum(void);
};

#endif