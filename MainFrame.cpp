#include "MainFrame.h"
#include <stdio.h>
#include <wx/gbsizer.h>
#include <wx/spinbutt.h>
#include <wx/xrc/xmlres.h>
#include "Hantek.h"

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_MENU(wxID_EXIT, MainFrame::OnExit)
    EVT_CLOSE(MainFrame::OnClose)
END_EVENT_TABLE()

MainFrame::MainFrame() {
    wxXmlResource::Get()->LoadFrame(this, NULL, "MainFrame");

    buttons["btnF0"] = KEY_F0;
    buttons["btnF1"] = KEY_F1;
    buttons["btnF2"] = KEY_F2;
    buttons["btnF3"] = KEY_F3;
    buttons["btnF4"] = KEY_F4;
    buttons["btnF5"] = KEY_F5;
    buttons["btnF6"] = KEY_F6;
    buttons["btnF7"] = KEY_F7;
    buttons["btnV0CCW"] = KEY_V0_CCW;
    buttons["btnV0CW"] = KEY_V0_CW;
    buttons["btnV0C"] = KEY_V0_C;
    buttons["btnSaveRecall"] = KEY_SAVE_RECALL;
    buttons["btnMeasure"] = KEY_MEASURE;
    buttons["btnAquire"] = KEY_ACQUIRE;
    buttons["btnUtility"] = KEY_UTILITY;
    buttons["btnCursor"] = KEY_CURSOR;
    buttons["btnDisplay"] = KEY_DISPLAY;
    buttons["btnAutoset"] = KEY_AUTOSET;
    buttons["btnSingSeq"] = KEY_SING_SEQ;
    buttons["btnRunStop"] = KEY_RUN_STOP;
    buttons["btnHelp"] = KEY_HELP;
    buttons["btnDefSetup"] = KEY_DEF_SETUP;
    buttons["btnSaveUsb"] = KEY_SAVE_USB;
    buttons["btnMathMenu"] = KEY_MATH_MENU;
    buttons["btnCh1Menu"] = KEY_CH1_MENU;
    buttons["btnCh1PosCcw"] = KEY_CH1_POS_CCW;
    buttons["btnCh1PosCw"] = KEY_CH1_POS_CW;
    buttons["btnCh1PosC"] = KEY_CH1_POS_C;
    buttons["btnCh1VoltsDivCcw"] = KEY_CH1_VOLTS_DIV_CCW;
    buttons["btnCh1VoltsDivCw"] = KEY_CH1_VOLTS_DIV_CW;
    buttons["btnCh2Menu"] = KEY_CH2_MENU;
    buttons["btnCh2PosCcw"] = KEY_CH2_POS_CCW;
    buttons["btnCh2PosCw"] = KEY_CH2_POS_CW;
    buttons["btnCh2PosC"] = KEY_CH2_POS_C;
    buttons["btnCh2VoltsDivCcw"] = KEY_CH2_VOLTS_DIV_CCW;
    buttons["btnCh2VoltsDivCw"] = KEY_CH2_VOLTS_DIV_CW;
    buttons["btnHorizMenu"] = KEY_HORIZ_MENU;
    buttons["btnHorizPosCw"] = KEY_HORIZ_POS_CW;
    buttons["btnHorizPosCcw"] = KEY_HORIZ_POS_CCW;
    buttons["btnHorizPosC"] = KEY_HORIZ_POS_C;
    buttons["btnHorizSecDivCcw"] = KEY_HORIZ_SEC_DIV_CCW;
    buttons["btnHorizSecDivCw"] = KEY_HORIZ_SEC_DIV_CW;
    buttons["btnTrigMenu"] = KEY_TRIG_MENU;
    buttons["btnTrigLevelCcw"] = KEY_TRIG_LEVEL_CCW;
    buttons["btnTrigLevelCw"] = KEY_TRIG_LEVEL_CW;
    buttons["btnTrigLevelC"] = KEY_TRIG_LEVEL_C;
    buttons["btnSetTo50"] = KEY_SET_TO_50;
    buttons["btnForceTrig"] = KEY_FORCE_TRIG;
    buttons["btnProbeCheck"] = KEY_PROBE_CHECK;

    std::map<std::string, uint8_t>::iterator iter = buttons.begin();
    while (iter != buttons.end()) {
        XRCCTRL(*this, iter->first.c_str(), wxButton)
            ->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                   wxCommandEventHandler(MainFrame::OnClickButton), this,
                   XRCID(iter->first.c_str()));

        iter++;
    }

    m_pHantek = new CHantek();
    m_pHantek->init();
    
    //startHantekThread();

    m_pHantek->readDSOSettings();

    updateStatus();
}

void MainFrame::startHantekThread(void) {
    if (CreateThread(wxTHREAD_JOINABLE) != wxTHREAD_NO_ERROR) {
        wxLogError("Could not create the worker thread!");
        return;
    }

    if (GetThread()->Run() != wxTHREAD_NO_ERROR) {
        wxLogError("Could not run the worker thread!");
        return;
    }
}

/*
MainDialog::MainDialog(const wxString & title)
       : wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 700))
{

    wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

    wxGridBagSizer* gbSizer = new wxGridBagSizer(8, 8);

    // Top


    // Menu
    wxStaticBoxSizer* sz = new wxStaticBoxSizer(wxVERTICAL, this, "Menu");
    sz->Add(new wxButton(sz->GetStaticBox(), wxID_ANY, wxT("F0"),
wxDefaultPosition, wxSize(30, 30)), 0, wxALL, 8); sz->Add(new
wxButton(sz->GetStaticBox(), wxID_ANY, wxT("F1"), wxDefaultPosition, wxSize(30,
30)), 0, wxLEFT | wxRIGHT | wxBOTTOM, 8); sz->Add(new
wxButton(sz->GetStaticBox(), wxID_ANY, wxT("F2"), wxDefaultPosition, wxSize(30,
30)), 0, wxLEFT | wxRIGHT | wxBOTTOM, 8); gbSizer->Add(sz, wxGBPosition(0, 0),
wxDefaultSpan, wxALL, 8);

    // Vertical
\
    //vbox->Add(panel, 1);
  //panel->SetSizer(hbox);
  //hbox->Add(createKnob(this, "CH1 Position"), 0, wxALL, 10);
  //hbox->Add(createKnob(this, "CH2 Position"), 0, wxALL, 10);
  vbox->Add(gbSizer, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
  ShowModal();
  Destroy();
}
*/

void MainFrame::OnClickButton(wxCommandEvent& event) {
    uint8_t key = 0;

    std::map<std::string, uint8_t>::iterator iter = buttons.begin();
    while (iter != buttons.end()) {
        if (event.GetId() == XRCID(iter->first.c_str())) {
            printf("%x\n", iter->second);
            key = iter->second;
            m_pHantek->keypressTrigger(key);
            m_pHantek->readDSOSettings();
            updateStatus();
            break;
        }

        iter++;
    }
}

void MainFrame::OnExit(wxCommandEvent& event) { Close(true); }

void MainFrame::updateStatus(void) {
    uint8_t buf[0xff];
    std::string sVal;

    //m_pHantek->getDSOSetting("[VERT-CH1-VB]", buf, nLen);

    char buf2[40];

    uint32_t nMVoltsPerDiv = m_pHantek->getMilliVoltsPerDiv(CHANNEL_1);
    if (nMVoltsPerDiv < 1000) {
        snprintf(buf2, sizeof(buf), "%umV", nMVoltsPerDiv);
    } else {
        snprintf(buf2, sizeof(buf), "%0.2fV", nMVoltsPerDiv / 1000.0);
    }

    XRCCTRL(*this, "lbVertCh1VoltsDiv", wxStaticText)->SetLabel("CH1 Volts / Div: "+std::string(buf2));

    wxChoice* c = XRCCTRL(*this, "chVertCh1Disp", wxChoice);
    c->SetSelection(c->FindString(m_pHantek->getChanDisplay(CHANNEL_1)));

    c = XRCCTRL(*this, "chVertCh1Coup", wxChoice);
    c->SetSelection(c->FindString(m_pHantek->getCoupling(CHANNEL_1)));
}


wxThread::ExitCode MainFrame::Entry() {
    while (!GetThread()->TestDestroy()) {
        if (m_bStopHantekIO) break;

        //m_pHantek->handleIO();
        // since this Entry() is implemented in MyFrame context we don't
        // need any pointer to access the m_data, m_processedData, m_dataCS
        // variables... very nice!
        // this is an example of the generic structure of a download thread:
        /*char buffer[1024];
        download_chunk(buffer, 1024);     // this takes time...
        {
            // ensure no one reads m_data while we write it
            wxCriticalSectionLocker lock(m_dataCS);
            memcpy(m_data+offset, buffer, 1024);
            offset += 1024;
        }
        // signal to main thread that download is complete
        wxQueueEvent(GetEventHandler(), new wxThreadEvent());*/
    }

    return (wxThread::ExitCode)0;
}

void MainFrame::OnClose(wxCloseEvent&) {
    m_bStopHantekIO = true;

    // important: before terminating, we _must_ wait for our joinable
    // thread to end, if it's running; in fact it uses variables of this
    // instance and posts events to *this event handler
    if (GetThread() &&      // DoStartALongTask() may have not been called
        GetThread()->IsRunning())
        GetThread()->Wait();

    Destroy();
}
