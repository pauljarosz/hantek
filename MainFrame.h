#ifndef __MAINFRAME_H__
#define __MAINFRAME_H__

#include <wx/wx.h>
#include <map>
#include <string>
#include "Hantek.h"

class MainFrame : public wxFrame, public wxThreadHelper {
   public:
    MainFrame();
    // MainDialog(const wxString& title);

   protected:
    virtual wxThread::ExitCode Entry();

   private:
    CHantek* m_pHantek;
    bool m_bStopHantekIO = false;
    std::map<std::string, uint8_t> buttons;
    void startHantekThread(void);
    void OnClickButton(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnClose(wxCloseEvent&);
    void updateStatus(void);

    wxDECLARE_EVENT_TABLE();
};

#endif
