#include "Timer.h"

CTimer::CTimer(int nTimeout) {
    m_nTimeout = nTimeout;

    reset();
}

void CTimer::reset(void) {
    m_Start = std::chrono::high_resolution_clock::now();
}

bool CTimer::isTimeout(void) {
    auto finish = std::chrono::high_resolution_clock::now();

    auto elapsed = finish - m_Start;
    auto i_millis = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed);

    if (i_millis.count() > m_nTimeout) {
        return true;
    }

    return false;
}