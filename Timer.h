#ifndef __TIMER_H__
#define __TIMER_H__

#include <chrono>

class CTimer {
   public:
    CTimer(int nTimeout);
    void reset(void);
    bool isTimeout(void);

   protected:
    int m_nTimeout;
    std::chrono::high_resolution_clock::time_point m_Start;
};

#endif