#include "main.h"
#include <wx/xrc/xmlres.h>
#include "MainFrame.h"

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit() {
    wxXmlResource::Get()->InitAllHandlers();

    bool bRet = wxXmlResource::Get()->Load("resource.xrc");
    if (!bRet) {
        //	cout  << "Could not load resource file" << Path << endl;
        return false;
    }

    wxImage::AddHandler(new wxPNGHandler);
    MainFrame *custom = new MainFrame();

    custom->Show(true);

    return true;
}

/*
#include "Hantek.h"
#include "HantekPacket.h"

int main() {
    
//p.print();
    CHantek hantek;
    hantek.init();
    //while(1) hantek.getNextPacket();
    //hantek.echo();
    
    //hantek.keypressTrigger(0x14);
    //hantek.readFile("/protocol.inf");
    //hantek.keypressTrigger(0x14);
    //hantek.keypressTrigger(0x14);
//    hantek.readDSOSettings();
    uint8_t len;
    uint8_t buf[30];
    hantek.getDSOSetting("[VERT-CH1-COUP]", buf, len );
    printf("\n\n%20x", buf[0]);
}
*/